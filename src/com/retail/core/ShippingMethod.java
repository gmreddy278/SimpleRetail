package com.retail.core;

public interface ShippingMethod {
	
	public double getItemShippingCost(Item item);
	
	public double getItemShippingCostWithTariff(Item item);
}
